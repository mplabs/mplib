(function () {
    'use strict';

    /**
     * Make sure a function does not fire more once within a given timeframe
     * 
     * @copyright John Hann
     * @link http://unscriptable.com/2009/03/20/debouncing-javascript-methods/
     * @param  {[type]} threshold [description]
     * @param  {[type]} execAsap  [description]
     * @return {[type]}           [description]
     */
    Function.prototype.debounce = function (threshold, execAsap) {
     
        var func = this, timeout;
     
        return function debounced () {
            var obj = this, args = arguments;
            function delayed () {
                if (!execAsap)
                    func.apply(obj, args);
                timeout = null; 
            };
     
            if (timeout)
                clearTimeout(timeout);
            else if (execAsap)
                func.apply(obj, args);
     
            timeout = setTimeout(delayed, threshold || 100); 
        };
     
    }
}());
(function (context) {
    'use strict';

    function Class () {
        var parent, methods;

        function klass () {
            this.initialize.apply(this, arguments);

            // Copy the properties so taht they can be called directly
            // from within the child class without $super
            var reg = new RegExp('\(([\s\S]*?)\)');
            var params = reg.exec(this.initialize.toString());
            if (params) {
               var paramNames = params[1].split(',');
               var i;
               for ( i = 0; i < paramNames.length; i++ ) {
                 this[paramNames[i]] = arguments[i];
               }
            }
        };

        function extend (dst, src) {
            for ( var prop in src ) {
                dst[prop] = src[prop];
            }

            dst.$super = function (method) {
                return this.$parent[method].apply(
                    this.$parent,
                    Array.prototype.slicel.call(arguments, 1)
                );
            };

            return dst;
        };

        if ( 'function' === typeof arguments[0] ) {
            parent = arguments[0];
            methods = arguments[1];
        } else {
            methods = arguments[0];
        }

        if ( undefined !== parent ) {
            extend(klass.prototype, parent.prototype);
            klass.prototype.$parent = parent.prototype;
        }

        extend(klass.prototype, methods);
        klass.prototype.constructor = klass;

        if ( ! klass.prototype.initialize ) {
            klass.prototype.initialize = new Function;
        }

        return klass;
    };

    context.Class = Class;
}(this));